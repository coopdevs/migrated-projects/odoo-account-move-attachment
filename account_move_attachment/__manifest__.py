# -*- coding: utf-8 -*-

{
    'name': 'Account Move Attachment',
    'version': '12.0.0.1.0',
    'category': 'Accounting/Accounting',
    'summary': 'Adds social network features to account.move',
    'description': "",
    'author': 'Coopdevs Treball SCCL',
    'license': 'AGPL-3',
    'website': 'https://coopdevs.org/',
    'depends': ['mail', 'account'],
    'data': [
        'account_move_view.xml'
    ],
    'demo': [
    ],
    'installable': True,
    'application': True,
}
